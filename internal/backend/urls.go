package backend

import "fmt"

func GenerateSeasonURL() (url string) {
	url = "https://statsapi.web.nhl.com/api/v1/seasons"
	return
}

func GenerateStarGamesURL(season string) (url string) {
	url = fmt.Sprintf("https://statsapi.web.nhl.com/api/v1/schedule?gameType=A&season=%s", season)
	return
}

func GenerateSeasonGamesURL(season string) (url string) {
	url = fmt.Sprintf("https://statsapi.web.nhl.com/api/v1/schedule?gameType=P&season=%s", season)
	return
}

func GenerateGamePlayersURL(game int) (url string) {
	url = fmt.Sprintf("https://statsapi.web.nhl.com/api/v1/game/%d/boxscore", game)
	return
}
