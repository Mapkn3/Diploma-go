package database

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/tern/migrate"
)

func Migrate() {
	Database.Do(func(conn *pgxpool.Conn) { migrateDatabase(conn.Conn()) })
}

func migrateDatabase(conn *pgx.Conn) {
	migrator, err := migrate.NewMigrator(context.Background(), conn, "schema_version")
	if err != nil {
		log.Fatalf("Unable to create a migrator: %v", err)
	}

	// err = migrator.LoadMigrations("./migrations")
	err = migrator.LoadMigrations("C:/Users/Mapkn3/go/src/gitlab.com/Mapkn3/Diploma-go/migrations")
	if err != nil {
		log.Fatalf("Unable to load migrations: %v", err)
	}

	err = migrator.Migrate(context.Background())
	if err != nil {
		log.Fatalf("Unable to migrate: %v", err)
	}

	ver, err := migrator.GetCurrentVersion(context.Background())
	if err != nil {
		log.Fatalf("Unable to get current schema version: %v", err)
	}

	log.Printf("Migration done. Current schema version: %v", ver)
}
