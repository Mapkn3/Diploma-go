package pkg

import (
	"Diploma-go/internal/backend/jsonconvert"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func ConvertPlayers(s map[int]jsonconvert.Player) string {

	re := regexp.MustCompile(`'`)
	items := make([]string, len(s))
	i := 0
	for k, v := range s {
		items[i] = fmt.Sprintf("('%s', '%s', '%s')", strconv.Itoa(k), v.Person.Season[:4]+"-"+v.Person.Season[4:], re.ReplaceAllString(v.Person.FullName, " "))
		i += 1
	}
	return strings.Join(items, ",\n")
}

func ConvertResults(ch map[string]int, s map[int]jsonconvert.Player) string {
	items := make([]string, len(s))
	for k, v := range s {
		items = append(items, fmt.Sprintf("('%d','%s','%d','%d','%d','%d','%d','%d','%d','%f','%d','%d','%d','%d','%d','%d','%d','%d','%s','%s','%s')",
			ch[strconv.Itoa(k)],
			v.Stats.TimeOnIce,
			v.Stats.Assists,
			v.Stats.Goals,
			v.Stats.Shots,
			v.Stats.Hits,
			v.Stats.PowerPlayGoals,
			v.Stats.PowerPlayAssists,
			v.Stats.PenaltyMinutes,
			v.Stats.FaceOffPct,
			v.Stats.FaceOffWins,
			v.Stats.FaceoffTaken,
			v.Stats.Takeaways,
			v.Stats.Giveaways,
			v.Stats.ShortHandedGoals,
			v.Stats.ShortHandedAssists,
			v.Stats.Blocked,
			v.Stats.PlusMinus,
			v.Stats.EvenTimeOnIce,
			v.Stats.PowerPlayTimeOnIce,
			v.Stats.ShortHandedTimeOnIce))
	}
	return strings.Join(items, ",\n")
}
