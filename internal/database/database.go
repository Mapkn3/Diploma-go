package database

import (
	"context"
	"log"
	"sync"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

var (
	once     sync.Once
	Database = database{url: dbURL}
)

type DatabaseTask func(*pgxpool.Conn)

type database struct {
	url  string
	pool *pgxpool.Pool
}

func (db *database) getPool() *pgxpool.Pool {
	once.Do(func() {
		pool, err := pgxpool.Connect(context.Background(), db.url)
		if err != nil {
			log.Fatalf("Unable to connection to database: %v", err)
		}
		db.pool = pool
	})

	return db.pool
}

func (db *database) Query(query string) (pgx.Rows, error) {
	pool := db.getPool()

	conn, err := pool.Acquire(context.Background())
	if err != nil {
		log.Fatalf("Unable to acquire a database connection: %v", err)
	}
	defer conn.Release()

	return conn.Query(context.Background(), query)
}

func (db *database) Exec(query string) error {
	pool := db.getPool()

	conn, err := pool.Acquire(context.Background())
	if err != nil {
		log.Fatalf("Unable to acquire a database connection: %v", err)
	}
	defer conn.Release()

	_, err = conn.Exec(context.Background(), query)
	return err
}

func (db *database) Do(task DatabaseTask) {
	pool := db.getPool()

	conn, err := pool.Acquire(context.Background())
	if err != nil {
		log.Fatalf("Unable to acquire a database connection: %v", err)
	}
	defer conn.Release()

	task(conn)
}
