package database

import (
	"Diploma-go/internal/backend/jsonconvert"
	"Diploma-go/pkg"
	"fmt"
)

func AddPlayers(players map[int]jsonconvert.Player) bool {
	//Drop all database strings
	if err := cleanResults(); err != nil {
		fmt.Printf("Unable delete: %v", err)
	}
	if err := cleanPlayers(); err != nil {
		fmt.Printf("Unable delete: %v", err)
	}

	err := Database.Exec("INSERT INTO players (playerid, season, name) VALUES" + pkg.ConvertPlayers(players))
	if err != nil {
		fmt.Printf("Unable to insert: %v", err)
		return false
	}

	err = Database.Exec(
		`INSERT INTO results (playerid
		, timeonice
		, assists
		, goals
		, shots
		, hits
		, powerplaygoals
		, powerplayassists
		, penaltyminutes
		, faceoffpct
		, faceoffwins
		, faceofftaken
		, takeaways
		, giveaways
		, shorthandedgoals
		, shorthandedassists
		, blocked
		, plusminus
		, eventimeonice
		, powerplaytimeonice
		, shorthandedtimeonice) VALUES` + pkg.ConvertResults(checkPlayers(), players))
	if err != nil {
		fmt.Printf("Unable to insert: %v", err)
		return false
	}
	return true
}

func GetPlayers() (players map[string][]*Player) {
	players = make(map[string][]*Player)
	rows, _ := Database.Query(
		`SELECT players.id
			, players.season
			, players.name
			, results.timeonice
			, results.assists
			, results.goals
			, results.shots
			, results.hits
			, results.powerplaygoals
			, results.powerplayassists
			, results.powerplaytimeonice
			, results.penaltyminutes
			, results.faceoffpct
			, results.faceoffwins
			, results.faceofftaken
			, results.takeaways
			, results.giveaways
			, results.shorthandedgoals
			, results.shorthandedassists
			, results.blocked
			, results.plusminus
			, results.eventimeonice
			, results.shorthandedtimeonice

			
			FROM players
			JOIN results ON players.id = results.playerid`)

	var p Player
	var season string
	for rows.Next() {
		err := rows.Scan(&p.Id, &season, &p.Name, &p.TimeOnIce, &p.Assists, &p.Goals, &p.Shots, &p.Hits, &p.PowerPlayGoals, &p.PowerPlayAssists,
			&p.PowerPlayTimeOnIce, &p.PenaltyMinutes, &p.FaceOffPct, &p.FaceOffWins, &p.FaceoffTaken, &p.Takeaways, &p.Giveaways,
			&p.ShortHandedGoals, &p.ShortHandedAssists, &p.Blocked, &p.PlusMinus, &p.EvenTimeOnIce, &p.ShortHandedTimeOnIce)

		if err != nil {
			fmt.Println("DB ERR:", err)
		}
		players[season] = append(players[season], &p)
	}
	return
}

func cleanResults() error {
	err := Database.Exec("DELETE FROM results")
	if err != nil {
		return err
	}
	return nil
}

func cleanPlayers() error {
	err := Database.Exec("DELETE FROM players")
	if err != nil {
		return err
	}
	return nil
}

func checkPlayers() (checkPlayers map[string]int) {

	checkPlayers = make(map[string]int)
	rows, _ := Database.Query(`SELECT players.id, players.playerid FROM players`)

	var Id int
	var PlayerId string
	for rows.Next() {
		if err := rows.Scan(&Id, &PlayerId); err != nil {
			fmt.Println("DB ERR:", err)
		}
		checkPlayers[PlayerId] = Id
	}
	return
}
