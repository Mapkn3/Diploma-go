package api

import (
	"io/ioutil"
	"log"
	"net/http"
)

//func for get content

func GetApiContent(url string) []byte {
	res, err := http.Get(url)
	if err != nil {
		log.Printf("error: %v", err)
	}
	if res.StatusCode != http.StatusOK {
		log.Printf(res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Printf("error: %v", err)
	}
	return body
}
