package database

import (
	"os"
)

var dbHost, _ = os.LookupEnv("DB_HOST")

var dbURL = "postgres://postgres:s3cr3t@" + dbHost + ":5432/nhl"
