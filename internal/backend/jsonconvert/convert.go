package jsonconvert

import (
	"encoding/json"
	"fmt"
)

//All func for convert JSON to go-struct

func ParseSeasonGames(raw []byte) (result Seasons) {
	if err := json.Unmarshal(raw, &result); err != nil {
		fmt.Println(err)
	}
	return
}

func ParseAllStar(raw []byte) (result AllStar) {
	if err := json.Unmarshal(raw, &result); err != nil {
		fmt.Println(err)
	}
	return
}

func ParseGamePlayers(raw []byte) (result GamePlayers) {
	if err := json.Unmarshal(raw, &result); err != nil {
		fmt.Println(err)
	}
	return
}

func ParseSeasonGame(raw []byte) (result SeasonGame) {
	if err := json.Unmarshal(raw, &result); err != nil {
		fmt.Println(err)
	}
	return
}
